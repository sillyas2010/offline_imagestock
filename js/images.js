
// Initialisation of files
function initFile(file) {

// генерируем новый объект FileReader
    let reader = new FileReader();

    // когда файл считывается он запускает событие OnLoad.
    if (file && file.type.match('image.*')) {
        reader.readAsDataURL(file);
    }
    else if( !file.type.match('image.*')) {
        alert(file.name + " isn't image, this file was't downloaded!");
        return;
    }

    // дабавляет атрибут src в изображение
    reader.onload = function(event) {
        the_url = event.target.result;
        initImage(file.name,the_url);
        renderImage(file.name,the_url);
    };

    reader.onloadend = function () {
        grid.packery('reloadItems').packery();
        grid.mCustomScrollbar("scrollTo", addImage);
    }

}

// Initialisation of images
function initImage(name,src) {
    let img = {
        imgName: name,
        src: src,
        comments: [],
        likes: 0,
        dislikes: 0,
        addComment: function (author,body) {
            let comment = {
                commentId: this.comments.length,
                nickname: author,
                comment: body,
                date: new Date().toLocaleString()
            };
            this.comments.push(comment);
        },


        setReputation: function(state) {
            if (state == "+") {
                this.likes++;
            }
            else if (state == "-") {
                this.dislikes++;
            }
        }
    };
    imagesInfo.push(img);
}

//Rendering images
function renderImage(name,src) {
    img = $("<article href='"+ src + "' class='grid-item' data-fancybox>" +
                "<img data-imgName='"+ name +"' src='" + src + "' />" +
            "</article>");
    img.insertBefore("#add-image");
}


