let inpFile = $("#the-file-input");
let grid = $('.grid');
let gridItem = $(".grid-item img");
let addImage = $("#add-image");
let imagesInfo = [];

$("h1").click(function () {
    inpFile.click();
});

grid.packery({
    itemSelector: '.grid-item',
    isHorizontal: true,
    gutter: 10,
    appended: addImage
});

$(function()
{
    $("#slide").hide();
    $("body").mCustomScrollbar({
        axis:"x",
        theme:"3d-dark",
        autoExpandScrollbar:true,
        scrollInertia: 0,
        advanced:{autoExpandHorizontalScroll:true
        }
    });

});

grid.on("click", ".grid-item", function() {
    grid.append($(".block-info"));
});

/*
grid.on("mouseleave", gridItem, function() {
    $(this).hide($(".block-info"));
});
*/

// изменение в input
inpFile.change(function() {
    //forEach() function for htmlCollection
    [].forEach.call(this.files,function(element) {
        initFile(element);
    });
    grid.packery('appended', addImage);
});
